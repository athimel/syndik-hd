#!/bin/bash
rm dist/* -rf
cp -rv raw/*.png dist/
cp -v chef.png dist/
cd dist
ls *.png | awk -F "." '{print "convert "$1".png -resize 80x80 "$1"_80.png ; convert "$1".png -resize 100x100 "$1"_100.png ; convert "$1".png -resize 150x150 "$1"_150.png ; convert "$1".png -resize 200x200 "$1"_200.png ; convert "$1".png -resize 300x300 "$1"_300.png"}' > /tmp/commands
bash /tmp/commands
rm /tmp/commands
echo "done."

